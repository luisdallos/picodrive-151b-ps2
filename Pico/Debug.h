
char *PDebugMain(void);
char *PDebugSpriteList(void);
void PDebugShowSpriteStats(unsigned short *screen, int stride);
void PDebugShowPalette(unsigned short *screen, int stride);
void PDebugShowSprite(unsigned short *screen, int stride, int which);
void PDebugDumpMem(void);

